# Contributing

There are several ways to contribute to the project, here are a few ones.


## Translate the application

Don't worry, this one is easy ! We use Transifex, which is a simple platform for translating applications. We have all the [labels there](https://www.transifex.com/openlevelup/whatosm/), in English, so you can write translations into your language. Let us know when you're done, that way we will update the application to make your language available.

If you run into any issue for doing this (like your language isn't available in the list), please [contact us](mailto:panieravide@riseup.net) or [open an issue](https://framagit.org/PanierAvide/WhatOSM/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).


## Add a new contribution tool

This application has a non-exhaustive list of contribution tools of the OSM ecosystem. If you want to add your own contribution tool in the list, you should meet some criterias:
* Tool must be __free-to-use__, and preferably libre/open source
* It must be __OpenStreetMap-related__, but might not be an OSM data editor (for example, qualifying aerial imagery for OSM editing is great)
* That's all.

If you want to add another contribution tool, there are two ways to proceed.


### For everyone - Open an issue

You can open an issue to propose a new tool. This is easy, and can be done by everyone. Just follow these steps:
* First, check if there is not an existing issue for the tool you want to add on [the issues list](https://framagit.org/PanierAvide/WhatOSM/issues).
* If not, create a [new issue](https://framagit.org/PanierAvide/WhatOSM/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) (may require to be logged in Framagit).
* Copy/paste this issue template, and fill in the blanks.

```
* Name of the tool: ___
* Catchphrase (in less than 50 characters): ___
* URL of the tool logo: https://___
* URL of a picture of the tool: https://___
* Description (not too short): ___
* URL of the page where you can start using the tool: https://___
* URL of a documentation page (can be OSM wiki): https://___
* Minimal required time to contribute: few seconds ? few minutes ? few hours ? few days ?
* Where you can use the tool: outside (smartphone, needing ground survey) ? inside (on desktop, not needing ground survey) ? everywhere ?
* Level of knowledge needed to use it: beginner (everyone can do) ? intermediate (some OSM knowledge needed) ? advanced (regular OSM contributor) ? expert (OSM guru) ?
```

* Submit issue, and when a developer will have time, it will be added.


### For developers - Create a pull request

You can also add the tool to the list by yourself, if you know a bit about JSON and Git. To do so, just follow these steps:
* Fork the repository, and create a new branch, based on `develop`. The branch must be named `feature/tool-NAMEOFTHETOOL`.
* Go to `src/config/tools` folder. You will see a bunch of JSON files. Each one is the description of a different contribution tool.
* Create a new JSON file for your project. It must be named only using alphanumeric characters, `-` and `_`.
* Complete the tool description, see [configuration documentation](CONFIG.md) for details and available values.
* Check if your file is valid by running `npm run build:tooljson` at the root of the project.
* If everything is OK, commit your changes, and create [a pull request](https://framagit.org/PanierAvide/WhatOSM/merge_requests).
* When some developer will have time, changes will be valited and merged into main project.


## Solve issues/develop features

If you are a developer, you can see the [list of open issues](https://framagit.org/PanierAvide/WhatOSM/issues) and start working on one of them. To start development, please read [development documentation](DEVELOP.md).
