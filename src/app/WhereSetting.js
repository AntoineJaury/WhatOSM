import React, {Component} from 'react';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import { Step, Stepper, StepButton } from 'material-ui/Stepper';
import NaturePeopleIcon from 'material-ui/svg-icons/image/nature-people';
import HomeIcon from 'material-ui/svg-icons/action/home';
import muiThemeable from 'material-ui/styles/muiThemeable';

const styles = {
	radioButton: {
		marginBottom: 15
	}
};

/**
 * WhereSetting is the component for selecting where the task should take place.
 */
class WhereSetting extends Component {
	constructor(props, context) {
		super(props, context);
		
		this.state = {};
	}
	
	handleChange(event, value) {
		if(this.props.onChange) {
			this.props.onChange(value);
		}
	}
	
	render() {
		return (
			<RadioButtonGroup style={this.props.style} name="where" defaultSelected={this.props.defaultValue} onChange={this.handleChange.bind(this)}>
				<RadioButton
					value="outside"
					label={I18n.t("Outside, on the ground")}
					checkedIcon={<NaturePeopleIcon color={this.props.muiTheme.palette.primaryColor1} />}
					uncheckedIcon={<NaturePeopleIcon />}
					style={styles.radioButton}
				/>
				<RadioButton
					value="inside"
					label={I18n.t("Remotely, from your home")}
					checkedIcon={<HomeIcon color={this.props.muiTheme.palette.primaryColor1} />}
					uncheckedIcon={<HomeIcon />}
					style={styles.radioButton}
				/>
			</RadioButtonGroup>
		);
	}
}

export default muiThemeable()(WhereSetting);
