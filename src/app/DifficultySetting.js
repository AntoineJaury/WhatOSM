import React, {Component} from 'react';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import { Step, Stepper, StepButton } from 'material-ui/Stepper';

const styles = {
	radioButton: {
		marginBottom: 15
	}
};

/**
 * DifficultySetting is the component for selecting the user expertise.
 */
class DifficultySetting extends Component {
	constructor(props, context) {
		super(props, context);
		
		this.state = {};
	}
	
	handleChange(event, value) {
		if(this.props.onChange) {
			this.props.onChange(value);
		}
	}
	
	render() {
		return (
			<RadioButtonGroup style={this.props.style} name="difficulty" defaultSelected={this.props.defaultValue} onChange={this.handleChange.bind(this)}>
				<RadioButton
					value="easy"
					label={I18n.t("Beginner")}
					style={styles.radioButton}
				/>
				<RadioButton
					value="intermediate"
					label={I18n.t("Intermediate")}
					style={styles.radioButton}
				/>
				<RadioButton
					value="difficult"
					label={I18n.t("Advanced")}
					style={styles.radioButton}
				/>
				<RadioButton
					value="complex"
					label={I18n.t("Expert")}
					style={styles.radioButton}
				/>
			</RadioButtonGroup>
		);
	}
}

export default DifficultySetting;
